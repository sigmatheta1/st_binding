# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Wrapper over sigmatheta library.
"""

import sys
import ctypes as ct
import numpy as np
from st_binding import libst


c_int_10 = ct.c_int * 10
c_double_10 = ct.c_double * 10
c_double_128 = ct.c_double * 128
c_int_128 = ct.c_int * 128
c_ulong_128 = ct.c_ulong * 128
p_c_int = np.ctypeslib.ndpointer(dtype=ct.c_int, ndim=1, flags="C_CONTIGUOUS")
p_c_double = np.ctypeslib.ndpointer(dtype=ct.c_double, ndim=1, flags="C_CONTIGUOUS")
p_c_ubyte = np.ctypeslib.ndpointer(dtype=ct.c_ubyte, ndim=1, flags="C_CONTIGUOUS")


st_asymptote_coeff = ct.c_double * 10


class st_conf_int(ct.Structure):
    _fields_ = [
        ("bmin2s", ct.c_double),  # 95 % confidence low value
        ("bmin1s", ct.c_double),  # 68 % confidence low value
        ("bmax1s", ct.c_double),  # 68 % confidence high value
        ("bmax2s", ct.c_double),
    ]  # 95 % confidence high value


st_conf_int_128 = st_conf_int * 128


class st_serie(ct.Structure):
    _fields_ = [
        ("tau", c_double_128),  # Tau serie
        ("dev", c_double_128),  # Deviation estimate
        ("dev_unb", c_double_128),  # Unbiased estimate
        ("alpha", c_int_128),  # Dominating power law
        ("asym", st_asymptote_coeff),  # Asymptotes coefficients
        ("conf_int", st_conf_int_128),  # Confidence intervals
        ("length", ct.c_size_t),  # Length of serie
        ("tau_base", ct.c_double),  # Base Tau value
        ("tau_norm", c_ulong_128),
    ]  # Normalized Tau value


class st_tau_inc(ct.Structure):
    _fields_ = [("tau", c_int_10), ("length", ct.c_size_t)]

    def __init__(self, tau=c_int_10(1, 0, 0, 0, 0, 0, 0, 0, 0, 0), length=1):
        super().__init__(tau, length)


class st_psd(ct.Structure):
    _fields_ = [
        ("ff", p_c_double),  # Fourrier frequency values array
        ("syy", p_c_double),  # PSD values array
        ("length", ct.c_size_t),
    ]  # Length of array


p_st_serie = ct.POINTER(st_serie)
p_st_psd = ct.POINTER(st_psd)


lib_st_serie_dev = libst.st_serie_dev
lib_st_serie_dev.argtypes = [p_st_serie, ct.c_int, ct.c_size_t, p_c_double]
lib_st_serie_dev.restype = ct.c_int


def st_serie_dev(serie, devtype, n, y):
    """Compute deviation of serie.
    :param serie: Pointer to st_serie data structure ()
    :param devtype: index (see deviance_type) of deviation type to use (int)
    :param n: length of 1D arrays (int).
    :param y: a 1D array containing frequency fluctuation data (numpy.array)
    :returns: 0 in case of successful completion (int)
    """
    y = np.ascontiguousarray(y, dtype=np.double)
    y = y.astype(np.double)
    return lib_st_serie_dev(ct.byref(serie), ct.c_int(devtype), ct.c_size_t(n), y)


lib_st_relatfit = libst.st_relatfit
lib_st_relatfit.argtypes = [p_st_serie, p_c_double, ct.c_int, p_c_ubyte, ct.c_ubyte]
lib_st_relatfit.restype = ct.c_int


def st_relatfit(serie, war, ord_, flag_slope, flag_bias):
    """ """
    war = np.ascontiguousarray(war, dtype=np.double)
    flag_slope = np.ascontiguousarray(flag_slope, dtype=np.dtype("B"))
    return lib_st_relatfit(ct.byref(serie), war, ord_, flag_slope, flag_bias)


lib_st_asym2alpha = libst.st_asym2alpha
lib_st_asym2alpha.argtypes = [p_st_serie, ct.c_ubyte]
lib_st_asym2alpha.restype = ct.c_int


def st_asym2alpha(serie, flag_variance):
    """ """
    return lib_st_asym2alpha(ct.byref(serie), flag_variance)


lib_st_avardof = libst.st_avardof
lib_st_avardof.argtypes = [p_st_serie, p_c_double, ct.c_ubyte]
lib_st_avardof.restype = ct.c_int


def st_avardof(serie, edf, flag_variance):
    """ """
    return lib_st_avardof(ct.byref(serie), edf, flag_variance)


lib_st_aduncert = libst.st_aduncert
lib_st_aduncert.argtypes = [p_st_serie, p_c_double]
lib_st_aduncert.restype = ct.c_int


def st_aduncert(serie, edf):
    """ """
    return lib_st_aduncert(ct.byref(serie), edf)


lib_st_psdgraph = libst.st_psdgraph
lib_st_psdgraph.argtypes = [p_st_psd, p_c_double, ct.c_double, ct.c_int, ct.c_int]
lib_st_psdgraph.restypes = ct.c_int


def st_psdgraph(y, tstep, ny, decim=False):
    """Power Spectral Density (PSD) calculation.
    Compute the Power Spectral Density of the 'ny' normalized
    frequency deviation elements of the vector 'y'  frequency deviation)
    versus the Fourrier frequency.
    :param t: Array of timetag data
    :param y: Array of normalized frequency deviation samples
    :param ny: Number of elements of samples used in the computation
    :param decim: Decimation flag default False (boolean)
    :returns: Arrays of Fourrier frequency values and PSD values
              (np.array of float, np.array of float)
    """
    psd = st_psd()
    y = np.ascontiguousarray(y, dtype=np.double)
    decim = 0 if decim is False else 1
    #
    if lib_st_psdgraph(ct.byref(psd), y, tstep, ny, decim):
        raise RuntimeError("PSD computation error")
    #
    # From https://stackoverflow.com/questions/4355524/getting-data-from-ctypes-array-into-numpy
    ff = ct.cast(psd.ff, ct.POINTER(np.ctypeslib.as_ctypes_type(np.float64)))
    syy = ct.cast(psd.syy, ct.POINTER(np.ctypeslib.as_ctypes_type(np.float64)))
    ff = np.ctypeslib.as_array(ff, shape=(psd.length,))
    syy = np.ctypeslib.as_array(syy, shape=(psd.length,))
    #
    return ff[:-1], syy[:-1]


lib_stu_populate_tau_list = libst.stu_populate_tau_list
lib_stu_populate_tau_list.argtypes = [
    p_st_serie,
    ct.c_size_t,
    ct.c_int,
    ct.c_double,
    ct.c_int,
    p_c_int,
]
lib_stu_populate_tau_list.restypes = ct.c_int


def populate_tau_list(serie, n, dev_type, tau_base, tau_inc_type, pattern=[0]):
    """Generate the real list of Tau in a serie structure
    :param serie: Pointer to st_serie data structure.
    :param n: Number of data samples
    :param dev_type: Deviation type to compute ('ADEV', 'MDEV'...).
    :param tau_inc_type: Tau increment type
    :param pattern: Array of 10 integer
    if increment of log type, the first value of the array is used as base.
    else, the array is used as a 'decade' pattern (see st_tau_inc_type).
    Use a '0' to signify the end of the pattern, e.g.:
    if pattern = {1, 2, 4, 0, 6, 7, 0, 0, 8, 0}
    the generated Tau list is in the form 1 2 4 10 20 40 100 200 400 1000 ...
    The value serie must be growing else error is returned.
    Each value must in the range [1 - 9] else error is returned.
    :returns: 0 in case of succefull completion,
              -1 if error in pattern syntax,
              -2 in other case.
    """
    pattern = np.ascontiguousarray(pattern, dtype=np.int32)
    return lib_stu_populate_tau_list(
        ct.byref(serie), n, dev_type, tau_base, tau_inc_type, pattern
    )
