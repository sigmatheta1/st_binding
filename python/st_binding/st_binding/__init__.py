# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022-2024
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Wrapper over sigmatheta library.
"""
import sys
import ctypes as ct
import ctypes.util as ctu
from collections import OrderedDict


LIBNAME = "sigmatheta"

DEVIANCE_TYPE = OrderedDict(
    [("ADEV", 0), ("MDEV", 1), ("HDEV", 2), ("PDEV", 3), ("GDEV", 4), ("HCDEV", 5)]
)


TAU_INC_TYPE = OrderedDict(
    [
        ("DEC", 0),  # Increment by decade
        ("OCT", 1),  # Increment by octave
        ("ALL", 2),  #  "All" incrementation
        ("LOG", 3),  #  Logarithm increment
        ("USR", 4),  #  User defined incrementation
    ]
)


def _load_library(libname):
    """Portable load library function. Should be compatible with Cygwin,
    Windows and Cdecl compatible system (Solaris, Linux, FreeBSD, macOS...).
    """
    if sys.platform != "cygwin":
        pathname = ctu.find_library(libname)
        if sys.platform == "win32":
            # Windows backend uses stdcall calling convention
            lib = ct.WinDLL(pathname)
        else:
            # Standard cdecl calling convention
            lib = ct.CDLL(pathname)
    else:
        # Cygwin predefines library names with 'cyg' instead of 'libst.'
        try:
            lib = ct.CDLL("cyg" + libname + ".dll")
        except Exception as ex:
            raise OSError("Library could not be found") from ex
    return lib


libst = _load_library(LIBNAME)
